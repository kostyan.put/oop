﻿using System;


namespace Oop.Shapes.Factories
{
	public class ShapeFactory
	{
		public Shape CreateCircle(int radius)
		{
			if (radius <= 0)
				throw new ArgumentOutOfRangeException("argument less or equals 0");

			Circle shape = new Circle();
			shape.VertexCount = 0;
			shape.Area = Math.PI * (radius * radius);
			shape.Perimeter = 2 * Math.PI * radius;
			
			return shape;

		}

		public Shape CreateTriangle(int a, int b, int c)
		{
			if (a <= 0 || b <= 0 || c <= 0)
				throw new ArgumentOutOfRangeException("one of arguments less or equals 0");

			if (a + b < c || b + c < a || a + c < b)
				throw new InvalidOperationException("the length of one side of a triangle cannot be greater than the sum of the lengths of the other two");

			Triangle triangle = new Triangle();
			triangle.VertexCount = 3;
			triangle.Perimeter = a + b + c;
			var SemiPerimeter = triangle.Perimeter / 2;
			triangle.Area = Math.Sqrt(SemiPerimeter * (SemiPerimeter - a) * (SemiPerimeter - b) * (SemiPerimeter - c));
			
			return triangle;

		}

		public Shape CreateSquare(int a)
		{
			if (a <= 0)
				throw new ArgumentOutOfRangeException("argument less or equals 0");

			Square square = new Square();
			square.VertexCount = 4;
			square.Perimeter = a * square.VertexCount;
			square.Area = a * a;

			return square;

		}

		public Shape CreateRectangle(int a, int b)
		{
			if (a <= 0 || b <= 0)
				throw new ArgumentOutOfRangeException("one of arguments less or equals 0");

			Rectangle rectangle = new Rectangle();
			rectangle.VertexCount = 4;
			rectangle.Perimeter = a * 2 + b * 2;
			rectangle.Area = a * b;

			return rectangle;

		}
	}
}